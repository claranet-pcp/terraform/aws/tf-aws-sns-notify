tf-aws-sns-notify
-----

Sends events sent to an SNS topic to Slack and/or E-Mail via SES. Supports events sent by CloudWatch.

## Terraform Version Compatibility
Module Version|Terraform Version
---|---
v1.0.0|0.12.x
v0.1.3|0.11.x

## Usage
-----

```js
// Create an SNS topic and send its events to a Slack channel.

module "alerts" {
  source = "../modules/tf-aws-sns-notify"

  name          = "${var.envname}-alerts"
  slack_url     = var.slack_url
}

```

Message key/values
------------------

###### Common
 - `Subject` (Required) - The user name or subject of Slack/E-Mail.
 - `Description` (Required) - The description of the alert.
 - `ShortDescription` (Required) - A short description of the alert.
 - `AlertMethod` - Currently either `SLACK` or `EMAIL`. Omitting key defaults to both

###### Slack
 - `IconEmoji` - The main icon used in the Slack message.
 - `Colour` - Colour used in the body of the message.

###### E-Mail
- `DescriptionHTML` (Required) - HTML formatted description.

###### Example

```
aws sns publish --topic-arn arn:aws:sns:eu-west-1:441829189711:sns-test --message "{\"Subject\":\"Test\",\"IconEmoji\":\":party_wizard:\",\"Description\":\"Testing\",\"StatusEmoji\":\":party_parrot:\",\"Status_description\":\"Testing\"}"
```

Variables
---------
_Variables marked with **[*]** are mandatory._

###### General variables
 - `name` - The name for resources created by this module. **[*]**
 - `slack_url` - The Slack webhook URL to send messages to. []
 - `email_recipient` - The E-Mail address to send messages to. []
 - `email_sender` - The E-Mail address to send messages from. []
 - `ses_region` - The region to send E-Mail from. [Default: `eu-west-1`]
 - `tags` - List of tags to add to the Lambda function this module uses to send slack messages. [Default: `{}`]

###### OK state variables (CloudWatch defaults)
 - `cw_ok_subject` - The subject to use when sending `OK` type messages. [Default: `CloudWatch`]
 - `cw_ok_user_emoji` - The icon for the user when sending `OK` type messages. [Default: `:aws:`]
 - `cw_ok_colour` - The colour to use for `OK` type messages. [Default: `:white_check_mark:`]

###### ALARM state variables (CloudWatch defaults)
 - `cw_alarm_subject` - The subject to use when sending `ALARM` type messages. [Default: `CloudWatch`]
 - `cw_alarm_user_emoji` - The icon for the user when sending `ALARM` type messages. [Default: `:aws:`]
 - `cw_alarm_colour` - The colour to use for `ALARM` type messages. [Default: `:x:`]

###### INSUFFICIENT_DATA state variables (CloudWatch defaults)
 - `cw_insufficient_data_subject` - The subject to use when sending `INSUFFICIENT_DATA` type messages. [Default: `CloudWatch`]
 - `cw_insufficient_data_user_emoji` - The icon for the user when sending `INSUFFICIENT_DATA` type messages. [Default: `:aws:`]
 - `cw_insufficient_data_colour` - The colour to use for `INSUFFICIENT_DATA` type messages. [Default: `:x:`]

<br />

Outputs
-------

 - `sns_topic_arn` - The ARN of the topic created used to send messages to for alerts.
