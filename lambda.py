"""
Receives SNS notifications.
Sends a message to a Slack Webhook URL with information about the event and optionally sends this in an E-Mail.

"""

import boto3
import json
import logging
import os

from botocore.exceptions import ClientError
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError


cloudwatch = boto3.client('cloudwatch')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

SLACK_URL = os.environ['SLACK_URL']
EMAIL_RECIPIENT = os.environ['EMAIL_RECIPIENT']
EMAIL_SENDER = os.environ['EMAIL_SENDER']
SES_REGION = os.environ['SES_REGION']

CW_OK_SEND_EMAIL = os.environ['CW_OK_SEND_EMAIL'].lower() == 'true'
CW_OK_SEND_SLACK = os.environ['CW_OK_SEND_SLACK'].lower() == 'true'

CW_STATES = {
    'OK': {
        'subject': os.environ['CW_OK_SUBJECT'],
        'icon_emoji': os.environ['CW_OK_USER_EMOJI'],
        'colour': os.environ['CW_OK_COLOUR'],
    },
    'ALARM': {
        'subject': os.environ['CW_ALARM_SUBJECT'],
        'icon_emoji': os.environ['CW_ALARM_USER_EMOJI'],
        'colour': os.environ['CW_ALARM_COLOUR'],
    },
    'INSUFFICIENT_DATA': {
        'subject': os.environ['CW_INSUFFICIENT_DATA_SUBJECT'] or os.environ['CW_ALARM_SUBJECT'],
        'icon_emoji': os.environ['CW_INSUFFICIENT_DATA_USER_EMOJI'] or os.environ['CW_ALARM_USER_EMOJI'],
        'colour': os.environ['CW_INSUFFICIENT_DATA_COLOUR'] or os.environ['CW_ALARM_COLOUR'],
    },
}

def lambda_handler(event, context):
    logger.info('Event: ' + str(event))
    data = json.loads(event['Records'][0]['Sns']['Message'])
    alert_data = parse_data(data)
    email = alert_data.get('send_email')
    slack = alert_data.get('send_slack')

    if email == True:
        send_email(alert_data)
    if slack == True:
        send_slack(alert_data)

def parse_data(data):
    alert_data = {}

    try:
        if 'NewStateValue' in data:
            new_state_value = data['NewStateValue']

            cw_state = CW_STATES[new_state_value]

            alert_data['subject'] = cw_state['subject']
            alert_data['description'] = data['AlarmDescription']
            alert_data['short_description'] = data['NewStateValue']

            alert_data['icon_emoji'] = cw_state['icon_emoji']
            alert_data['description_html'] = data['AlarmDescription']
            alert_data['colour'] = cw_state['colour']

            # Default sending on whether a destination is provided
            alert_data['send_email'] = EMAIL_RECIPIENT != ""
            alert_data['send_slack'] = SLACK_URL != ""

            if new_state_value == 'OK':
                if alert_data['send_email']:
                    # Send a an email OK? Defaults to true in tfvars
                    alert_data['send_email'] = CW_OK_SEND_EMAIL
                if alert_data['send_slack']:
                    # Send a Slack OK? Defaults to true in tfvars
                    alert_data['send_slack'] = CW_OK_SEND_SLACK
        else:
            alert_data['subject'] = data.get('Subject')
            alert_data['description'] = data.get('Description')
            alert_data['short_description'] = data.get('ShortDescription')

            alert_data['icon_emoji'] = data.get('IconEmoji')
            alert_data['colour'] = data.get('Colour')
            alert_data['description_html'] = data.get('DescriptionHTML')

            alert_data['send_email'] = False
            alert_data['send_slack'] = False

            # check to see if an alert method is in the message. used to control alert method for each message. more can be added
            if 'AlertMethod' in data:
                alert_method = data.get('AlertMethod')
                if 'EMAIL' in alert_method:
                    if EMAIL_RECIPIENT:
                        alert_data['send_email'] = True
                    else:
                        logger.error('Alert requested email but no recipient address provided')
                if 'SLACK' in alert_method:
                    if SLACK_URL:
                        alert_data['send_slack'] = True
                    else:
                        logger.error('Alert requested slack but no url provided')
            else:
                if EMAIL_RECIPIENT:
                    alert_data['send_email'] = True
                if SLACK_URL:
                    alert_data['send_slack'] = True

    except KeyError as e:
        raise Exception('Required key not found: ' + str(e))

    return alert_data

def send_slack(data):
    data = {
        'username': data['subject'],
        'icon_emoji': data['icon_emoji'],
        'attachments': [{
            'title': data['short_description'],
            'text': '{description}'.format(
                description=data['description'],
                ),
            'color': data['colour']
            }],
    }

    post_data = json.dumps(data).encode('utf-8')

    req = Request(SLACK_URL, post_data)
    try:
        response = urlopen(req)
        response.read()
        logger.info('Message posted to %s', SLACK_URL)
    except HTTPError as e:
        logger.error('Request failed: %d %s', e.code, e.reason)
    except URLError as e:
        logger.error('Server connection failed: %s', e.reason)


def send_email(data):
    ses = boto3.client('ses',region_name=SES_REGION)
    charset = 'UTF-8'

    try:
        response = ses.send_email(
            Destination={
                'ToAddresses': [
                    EMAIL_RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': charset,
                        'Data': data['description_html'],
                    },
                    'Text': {
                        'Charset': charset,
                        'Data': data['description'],
                    },
                },
                'Subject': {
                    'Charset': charset,
                    'Data': data['subject'] + ' ' + data['short_description'],
                },
            },
            Source=EMAIL_SENDER,
        )

    except ClientError as e:
        logger.error(e.response['Error']['Message'])
    else:
        logger.info("Email sent! Message ID:"),
        logger.info(response['ResponseMetadata']['RequestId'])
