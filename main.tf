# Create the Lambda function.

module "lambda" {
  source = "git::https://github.com/claranet/terraform-aws-lambda.git?ref=v1.1.0"

  function_name = var.name
  description   = "Sends CloudWatch Alarm events to Slack and E-Mail"
  handler       = "lambda.lambda_handler"
  runtime       = "python3.6"
  timeout       = 10

  tags = var.tags

  source_path = "${path.module}/lambda.py"

  policy = {
    json = data.aws_iam_policy_document.lambda.json
  }

  environment = {
    variables = {
      SLACK_URL       = var.slack_url
      EMAIL_RECIPIENT = var.email_recipient
      EMAIL_SENDER    = var.email_sender
      SES_REGION      = var.ses_region

      CW_OK_SEND_EMAIL = var.cw_ok_send_email
      CW_OK_SEND_SLACK = var.cw_ok_send_slack

      CW_OK_SUBJECT    = var.cw_ok_subject
      CW_OK_USER_EMOJI = var.cw_ok_user_emoji
      CW_OK_COLOUR     = var.cw_ok_colour

      CW_ALARM_SUBJECT    = var.cw_alarm_subject
      CW_ALARM_USER_EMOJI = var.cw_alarm_user_emoji
      CW_ALARM_COLOUR     = var.cw_alarm_colour

      CW_INSUFFICIENT_DATA_SUBJECT    = var.cw_insufficient_data_subject
      CW_INSUFFICIENT_DATA_USER_EMOJI = var.cw_insufficient_data_user_emoji
      CW_INSUFFICIENT_DATA_COLOUR     = var.cw_insufficient_data_colour
    }
  }
}

data "aws_iam_policy_document" "lambda" {
  statement {
    effect = "Allow"

    actions = [
      "cloudwatch:DescribeAlarmHistory",
      "ses:SendEmail",
    ]

    resources = [
      "*",
    ]
  }
}

# Topic for alert messages to be sent to.

resource "aws_sns_topic" "sns_topic" {
  name = var.name
}

# Subscribe the Lambda function to the SNS topic.

resource "aws_sns_topic_subscription" "lambda" {
  topic_arn = aws_sns_topic.sns_topic.arn
  protocol  = "lambda"
  endpoint  = module.lambda.function_arn
}

# Add permission for SNS to execute the Lambda function.

resource "aws_lambda_permission" "sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.sns_topic.arn
}
