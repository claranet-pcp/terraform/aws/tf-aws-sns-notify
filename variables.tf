variable "name" {
  type        = string
  description = "The name to use for created resources"
}

variable "slack_url" {
  type        = string
  description = "The Slack webhook URL"
  default     = ""
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "cw_ok_send_slack" {
  type    = bool
  default = true
}

variable "cw_ok_send_email" {
  type    = bool
  default = true
}

# Display options for the OK state:

variable "cw_ok_subject" {
  type    = string
  default = "CloudWatch"
}

variable "cw_ok_user_emoji" {
  type    = string
  default = ":aws:"
}

variable "cw_ok_colour" {
  type    = string
  default = "#36a64f"
}

# Display options for the ALARM state:

variable "cw_alarm_subject" {
  type    = string
  default = "CloudWatch"
}

variable "cw_alarm_user_emoji" {
  type    = string
  default = ":aws:"
}

variable "cw_alarm_colour" {
  type    = string
  default = "#ff0000"
}

# Display options for the INSUFFICIENT_DATA state:

variable "cw_insufficient_data_subject" {
  type    = string
  default = "CloudWatch"
}

variable "cw_insufficient_data_user_emoji" {
  type    = string
  default = ":aws:"
}

variable "cw_insufficient_data_colour" {
  type    = string
  default = "#ff9900"
}

# E-Mail settings

variable "email_recipient" {
  type        = string
  description = "E-Mail address to send alerts to"
  default     = ""
}

variable "email_sender" {
  type        = string
  description = "E-Mail address to send alerts from"
  default     = ""
}

variable "ses_region" {
  type        = string
  description = "Region to send E-Mail from"
  default     = "eu-west-1"
}
